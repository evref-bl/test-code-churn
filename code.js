//Populate the welcome text
var is_new = false;
var name = (is_new ? "Hello New Customer" : "Hello Customer")
$("#welcome").text(name);

// Function to transform a string into a date
function transformStringToDate(string) {
    return new Date(string);
}

//Example usage of transformStringToDate function 
var dateString = "2022-01-01"; 
var date = transformStringToDate(dateString); 
console.log(date);